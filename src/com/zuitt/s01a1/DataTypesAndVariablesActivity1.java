package com.zuitt.s01a1;

public class DataTypesAndVariablesActivity1 {
    public static void main (String[] args) {

        String firstName = "Lordjel";

        String lastName = "Eleda";

        int subject = 3;
        int englishGrade = 90;
        int mathematicsGrade = 91;
        int scienceGrade = 92;

        int total = englishGrade + mathematicsGrade + scienceGrade;
        int average = total/subject;

        System.out.println("The average grade of " + firstName + " " + lastName + " is " + average);
    }
}
